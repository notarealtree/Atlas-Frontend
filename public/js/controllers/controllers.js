/**
 * Created by Francis on 3/3/2016.
 */

var recruitmentControllers = angular.module('recruitmentControllers', []);

recruitmentControllers.controller('MainCtrl', function($scope, apiFactory){
    var clipboard = new Clipboard('.btn');

    var keyIds = /\d{7}/g;
    var vCodes = /[\d\w]{64}/g;
    var attributes = /\[b]Attributes\[\/b]\n\[table][\S\s]*\[\/table]/g;

    var l1 = /\[img]https:\/\/community\.eveonline\.com\/bitmaps\/character\/level1.gif\[\/img]/g;
    var l2 = /\[img]https:\/\/community\.eveonline\.com\/bitmaps\/character\/level2.gif\[\/img]/g;
    var l3 = /\[img]https:\/\/community\.eveonline\.com\/bitmaps\/character\/level3.gif\[\/img]/g;
    var l4 = /\[img]https:\/\/community\.eveonline\.com\/bitmaps\/character\/level4.gif\[\/img]/g;
    var l5 = /\[img]https:\/\/community\.eveonline\.com\/bitmaps\/character\/level5.gif\[\/img]/g;

    var l1replace = '[img]http://eveboard.com/res/level1.gif[/img]';
    var l2replace = '[img]http://eveboard.com/res/level2.gif[/img]';
    var l3replace = '[img]http://eveboard.com/res/level3.gif[/img]';
    var l4replace = '[img]http://eveboard.com/res/level4.gif[/img]';
    var l5replace = '[img]http://eveboard.com/res/level5.gif[/img]';


    $scope.theirName = '';
    $scope.yourComment = '';

    $scope.yourColour = '#0066CC';
    $scope.theirColour = '#FF9900';
    $scope.yourName = '';

    /**
     *
     * @param {string} text
     */
    $scope.formatSkills = function(text){
        text = text.replace(attributes, '');
        text = text.replace(l1, l1replace);
        text = text.replace(l2, l2replace);
        text = text.replace(l3, l3replace);
        text = text.replace(l4, l4replace);
        text = text.replace(l5, l5replace);
        text = text.replace(/\n\n/, '');
        $scope.skills = text;
        $scope.updatePost();
    };

    /**
     *
     * @param {string} text
     */
    $scope.formatInterview = function(text){
        if($scope.yourName != ''){
            text = text.replaceAll($scope.yourName, colourString($scope.yourName, $scope.yourColour));
        }
        if($scope.theirName != ''){
            text = text.replaceAll($scope.theirName, colourString($scope.theirName, $scope.theirColour));
        }

        var cleanedText = [];
        text.split('\n').forEach(function(line){
            if(line.charAt(1) == '['){
                cleanedText.push(line);
            }
        });
        text = cleanedText.join('\n');
        text = cleanString(text);
        $scope.interview = text;
        $scope.updatePost();
    };

    function cleanString(input) {
        var output = '';
        for (var i=0; i<input.length; i++) {
            if (input.charCodeAt(i) <= 127) {
                output += input.charAt(i);
            }
        }
        return output;
    }

    function colourString(string, colour){
        return '[color=' + colour + ']' + string + '[/color]';
    }

    /**
     * Retrieves a list of key value combinations
     * @param text
     */
    $scope.findKeys = function (text){
        if(text.length == 0){
            return;
        }

        var keys = text.match(keyIds);
        var codes = text.match(vCodes);

        $scope.keyLength = keys.length;
        $scope.vCodeLength = codes.length;

        var kvPairs = [];
        for(var i = 0; i < keys.length; i++){
            kvPairs.push({
                'key': keys[i],
                'val': codes[i]
            })
        }

        apiFactory.checkApiKeys(kvPairs).then(function(keys){
            $scope.keys = keys;
            updateMail();
            $scope.updatePost();
        });
    };

    $scope.getZKillboard = function(){
        changeTitle();
        apiFactory.getZKillboard($scope.theirName).then(function(data){
            $scope.zkillboard = data;
            $scope.formatApplication();
        });
    };

    $scope.formatApplication = function(text){
        $scope.application = text;
        $scope.updatePost();
    };

    $scope.updatePost = function(){
        $scope.post =
            '[b]Interview[/b]\n[spoiler]\n' +
            $scope.interview +
            '\n[/spoiler]\n\n[b]Skills[/b]\n[spoiler]\n' +
            $scope.skills +
            '\n[/spoiler]\n\n[b]Application[/b]\n[spoiler]\n' +
            $scope.application +
            '\n[/spoiler]\n\n[b]zKillboard[/b]\n[spoiler]\n' +
            $scope.zkillboard +
            '\n[/spoiler]\n\n' +
            $scope.yourComment
        ;
    };

    function updateMail(){
        $scope.mail = 'Dear Tinfoilhatgang,\n\nApplicant Name: ' + $scope.theirName + ' \nPlease check the following keys for anything suspicious:\nkeyID,vCode\n';
        $scope.keys.forEach(function(key){
            $scope.mail += key.keyID + ',' + key.vCode + '\n';
        });
        $scope.mail += '\n Love you longtime <3';
    }

    function changeTitle(){
        var date = new Date();
        $scope.title = 'Application of ' + $scope.theirName + ' [' + leftPad(date.getDate()) + '/' + leftPad(date.getMonth() + 1) + '/' + date.getFullYear() + ']';
    }

    function leftPad(number){
        return number > 9 ? '' + number : '0' + number;
    }

    String.prototype.replaceAll = function(s,r){return this.split(s).join(r)};
});
