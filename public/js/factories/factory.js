/**
 * Created by Francis on 5/1/2016.
 */

var recruitmentFactories = angular.module('recruitmentFactories', ['ngResource']);

recruitmentFactories.factory('apiFactory', ['$http', '$q', function($http, $q){
    var keyIds = /\d{7}/g;
    var vCodes = /[\d\w]{64}/g;

    return {
        checkApiKeys    : checkApiKeys,
        getZKillboard   : getZKillboard
    };

    function checkApiKeys(kvPairs){
        var functions = [];
        kvPairs.forEach(function(pair){
            functions.push($http.get('https://api.eveonline.com/account/APIKeyInfo.xml.aspx?keyID=' + pair.key + '&vCode=' + pair.val));
        });

        var keys = [];
        return $q.all(functions).then(function(results){
            results.forEach(function(result){
                var parser = new DOMParser();
                var xml = parser.parseFromString(result.data, "text/xml");
                var json = xmlToJson(xml);

                var keyID = result.config.url.match(keyIds)[0];
                var vCode = result.config.url.match(vCodes)[0];

                var accessMask = json.eveapi.result.key['@attributes'].accessMask;

                keys.push({
                    keyID: keyID,
                    vCode: vCode,
                    accessMask: json.eveapi.result.key['@attributes'].accessMask,
                    expiry: json.eveapi.result.key['@attributes'].expires,
                    valid: ((accessMask == 4294967295 || accessMask == 1073741823) && json.eveapi.result.key['@attributes'].expires.length == 0)
                });
            });
            return keys;
        });
    }

    function getZKillboard(theirName){
        return $http.get('https://api.eveonline.com/eve/CharacterID.xml.aspx?names=' + encodeURI(theirName)).then(function(data){
            var parser = new DOMParser();
            var xml = parser.parseFromString(data.data, "text/xml");
            var json = xmlToJson(xml);
            var characterID = json.eveapi.result.rowset.row['@attributes'].characterID;
            return $http.get('https://zkillboard.com/api/stats/characterID/' + characterID + '/').then(function(data){
                console.log(data.data);

                var efficiency = 100 * (1 - (data.data.iskLost / data.data.iskDestroyed));
                efficiency = Math.round(efficiency * 100) / 100;

                var zkillboard =
                    'Kills: ' + data.data.shipsDestroyed +
                    ' Losses: ' + data.data.shipsLost + '\n' +
                    'Efficiency: ' + efficiency + '%\n' +
                    '[url]https://zkillboard.com/character/' + characterID + '/[/url]'
                ;
                return zkillboard;
            });
        });
    }

    function xmlToJson(xml) {
        var obj = {};

        if (xml.nodeType == 1) {
            if (xml.attributes.length > 0) {
                obj["@attributes"] = {};
                for (var j = 0; j < xml.attributes.length; j++) {
                    var attribute = xml.attributes.item(j);
                    obj["@attributes"][attribute.nodeName] = attribute.nodeValue;
                }
            }
        } else if (xml.nodeType == 3) {
            obj = xml.nodeValue;
        }

        if (xml.hasChildNodes()) {
            for(var i = 0; i < xml.childNodes.length; i++) {
                var item = xml.childNodes.item(i);
                var nodeName = item.nodeName;
                if (typeof(obj[nodeName]) == "undefined") {
                    obj[nodeName] = xmlToJson(item);
                } else {
                    if (typeof(obj[nodeName].push) == "undefined") {
                        var old = obj[nodeName];
                        obj[nodeName] = [];
                        obj[nodeName].push(old);
                    }
                    obj[nodeName].push(xmlToJson(item));
                }
            }
        }
        return obj;
    }
}]);