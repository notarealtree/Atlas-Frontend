'use strict';

var recruitmentApp = angular.module('recruitmentApp', [
    'ngRoute',
    'recruitmentControllers',
    'recruitmentFactories'
]);

recruitmentApp.config(['$routeProvider', '$locationProvider',
    function($routeProvider, $locationProvider) {
        $routeProvider.otherwise({
            redirectTo: '/',
            controller: 'MainCtrl',
            templateUrl: 'templates/form.html'
        });
        $locationProvider.html5Mode(true);
    }]);